# Using the DTH-Kit

The DTH-Kit can be used in various configurations which comprise different loop-back modes (see below) for testing purposes. Internally the DTH has two FireFly connectors which are interfaced on the front panel to a single MPO24 connector. The DTH-Kit contains a loopback-connector which has to be plugged into the MPO24 connector in order to do one of the loopback tests. Details about the fibre connectivity can be found in a previous [Hardware Section](installation.md#hardware).

!!! note 

    The TCP/IP data streams comes out of the DAQ 0 QSFP.

On the PC side sub-systems have to install a 100Gbps optical Ethernet NIC in the receiving PC. This card must be configured to work with Jumbo Frames. An example configuration file for  a 100Gbps NIC on a CENT OS machine is shown below. The configuration file is located in the directory `/etc/sysconfig/network-scripts` and it has the name `1ifcfg-{interface name}`. The interface name can be identified with the command ifconfig.
File contents:
```
TYPE=Ethernet
HWADDR=ec:0d:9a:a0:22:14
BOOTPROTO=none
DEFROUTE=no
IPV4_FAILURE_FATAL=no
IPV6INIT=no
DEVICE=p4p1
ONBOOT=yes
NM_CONTROLLED=no
IPADDR=192.168.1.200
NETMASK=255.255.255.0
MTU=9000
```
The first version of the DTH-Kit supports up to two incoming SLINK Rocket streams which are transferred to two independent TCP/IP streams over the same QSFP transceiver. 

##  SLINK Rocket data format

The data format for the SLINK Rocket link is documented in [EDMS Document 2502737](https://edms.cern.ch/ui/file/2502737/2/cms_phase2_slinkrocket.pdf).
