# User Guide of the provided software utilities

In the following the usage of the provided utilities are described.

##  DTH_Flashy.py

This utility allows to operate on the flash memories on the DTH board. There is one flash memory for the DAQ FPGA and one for the TCDS FPGA which can be individually addressed. A separate manual for this utility describes it's usage in [Flasy User Guide](https://cms-daq-dth-p1.web.cern.ch/sites/cms-daq-dth-p1.web.cern.ch/files/documents/Flashy.pdf)).

## Receiving the outgoing data stream of the DTH : dth_recv

The DTH is sending one or several TCP/IP data streams to the event builder of CMS. In laboratories it is required to receive and inspect this outgoing data stream with a stand alone programme. The dth_recv utility has been written for this purpose. The utility is available in the yum repository of the DTH software and should be installed on the machine hosting the NIC to receive the data from the DTH. The repository configuration file can be downloaded and installed with:
```
sudo curl https://gitlab.cern.ch/dth_p1-v2/cmd_dth.yum/raw/master/DTH-Kit_p1_v2.repo -o /etc/yum.repos.d/DTH-Kit_p1_v2.repo
```
Then the utility can be installed with:
```
yum install dth_recv-{version number}
```
The available packages in the repository can be listed with:
```
yum  repo-pkgs DTH-Kit_p1_v2 list
```
Finally the help text of the utility documents the usage:
```
dth_recv --help
```

reveals the various options and modes which this programme allows to use. The user may specify the port number to listen on for the incoming stream. If not provided the port number defaults to 10000 which is the destination port number of the default data streams from the `DTH_Control.py` utility (see below).

##  Control.py
DTH_Control is a menu driven programme which allows to setup the DTH for various data taking modes. These modes allow to test various parts of the SLINK Rocket link and the DTH data acquisition path. For all modes it is necessary to start a receiver programme on the computer which houses the 100Gb NIC which receives the TCP/IP data stream from the DTH. The DTH-Kit provides the `dth_recv` programme for this purpose (see above).
`DTH_Control.py` requires two configuration files in order to be able to operate. The first file contains the address table for the DAQ FPGA. The file is called DTHAddressMap.dat and contains an ASCII address table for the HAL library. The programme expects this file to be in 
In total four operational modes can be configured. They are described in the following section:
`/usr/local/share/dth_software_kit/DTHAddressMap.dat` (The programme first looks in the current directory for a file with this name. If the file is found it is used as the address table.  This can be used by developers to test new address tables.).
The second configuration file has to be present in the current working directory (where the `DTH_Control.py` programme was launched) and is called: `DTH_control_parameters.json`. This file contains configuration parameters for the TCP/IP connections to the DAQ computer receiving the TCP/IP stream of the DAQ link. An example file is contained in /`usr/local/share/dth_software_kit/DTH_control_parameters.json`. This file can be copied to the current working directory but it has to be edited according to the network configuration of the DAQ link. The file format (json) and the items to be defined in the file are listed below (the comments on the right side do not belong to the file): 
```
{
"sourceIp" : "192.168.01.16",  	The IP number of the DTH
"destIp" : "192.168.1.200",		The IP number of the receiving PC interface
"sourcePort0" : 10000,			The port number used by the DTH for TCP/IP stream 0
"destPort0" : 10000 ,			The port number used by the receiver for TCP/IP stream 0
"sourcePort1" : 10010,			The port number used by the DTH for TCP/IP stream 1
"destPort1" : 10000 ,			The port number used by the receiver for TCP/IP stream 1
"FF_CDR" : 1,				Defines if the CDR of the FireFly is used
"SR0" : 1, 				Enables Slink Rocket Channel 0 (and TCP/IP stream 0)
"SR1" : 1  				Enables Slink Rocket Channel 1 (and TCP/IP stream 1)
}
```

!!! note
    SR0 is the TCP/IP stream fed by inputs 13/24 of the MPO input connector.
    SR1 is the TCP/IP stream fed by inputs 14/23 of the MPO input connector.

### Internal Event Generator for DAQ link tests

This mode uniquely test the TCP/IP DAQ link from the DTH to a computer housing the NIC to receive the 100Gb TCP/IP data stream. Instead of receiving data from an SLINK Receiver core an event generator in the DTH generates event fragments and inserts them into the DAQ logic of the DTH.
![Internal Event Generator](images/DTH-Kit_DAQGenerator.png){: style="display: block; margin: 0 auto; height: 15em;"} 

### Loopback with internal Slink Rocket sender and Event Generator

In this mode the entire SLINK Rocket chain and Data acquisition link of the DTH can be tested. A loopback fibre has to be connected from the SLINK Rocket output to the Slink Rocket input. The DTH-Kit firmware contains the same SLINK Sender core as the one provided to subsystems for integration in their designs. The input of this sender core is connected to an event generator.
![Loopback with internal Slink Rocket sender and Event Generator](images/DTH-Kit_FullChain.png){: style="display: block; margin: 0 auto; height: 15em;"}

### Loopback with internal Slink Rocket sender using the Event Generator of the Slink Rocket sender IP

This mode is similar to the mode described in the previous section, however events are generated in the Event Generator of the Slink Rocket sender IP. This mode can also be used when the Receiving Slink Rocket port is connected to a Slink Rocket sender of the sub-system front end electronics. In the final DAQ system this mode will be used to test the connectivity of the sub-system to the DTH.
![Loopback with internal Slink Rocket sender using the Event Generator of the Slink Rocket sender IP](images/DTH-Kit_SenderIPGenerator.png){: style="display: block; margin: 0 auto; height: 15em;"}

### Connecting the Sub-detector front end boards: Standard DAQ mode

This mode is used for standard data acquisition. The receiver port is connected to the sub-detector fed and the DTH is configured to receive Slink Rocket data from this port.
![DAQ Mode](images/DTH-Kit_DAQMode.png){: style="display: block; margin: 0 auto; height: 15em;"} 
