# Using the software

The software of the DTH-Kit is a collection of Python, C++ and C programs. They are all packed into a single RPM which installs also non standard dependencies. In particular the following packages are installed: The PyHAL (a python wrapper for the HAL) and the python console-menu package which is used to display menus for various tools on the command. Technically python packages are provided as a wheel (a python package format) which are installed in the post-installation script of the DTH-kit rpm. 

In order to run the software your `LD_LIBRARY_PATH` needs to point to `opt/xdaq/lib`:
```
export LD_LIBRARY_PATH=/opt/xdaq/lib
```
Updates for the DTH-Kit software will be provided by the DAQ group in form of RPMs with a higher version number. They can be installed with the command:
```
rpm -U {path_to_rpm}
```
