# Introduction

In Phase II the interface between the sub-detector electronics and the central DAQ system is implemented via the DTH (DAQ and Timing Hub). This ATCA board provides sub-systems with timing and control signals (similar to the TCDS system during Phase I) and collects data from the sub-systems for each triggered events via optical links SLINK Rocket. 

To allow sub-systems to test their DAQ backend boards the DAQ Kit is provided. It consists of hardware and software allowing to test timing and control functionality of the back-end electronics and read out data from the back-end systems via the SLINK Rocket links. The DAQ Kit further provides simple software to initialise and control these functionalities.

The initial version of the DTH Kit provides a minimal set of functionalities. When the development of the firmware of the central DAQ group proceeds new functionalities will be made available also for the DTH Kit via firmware and software upgrades.
