#  Installation

The card will be handed out with an installation of the recommended operating system and user software. The hardware will be running the recommended firmware. Therefore no installation is necessary by the user.
In future the functionality of the DTH kit will be upgraded. The upgrades will be provided in form of firmware and software upgrades. For information the following sections give details about the installed software. 

## Hardware

The DTH Kit will be equipped with FireFly optics which is brought to one of the MPO-24 connectors on the front-panel. To use the DTH-Kit the following additional hardware equipment is necessary:
- Two [100Gbps QFSP](https://www.fs.com/de/products/48354.html) transceivers for the data transfer to the DAQ PC.
- A [100Gbit fibre](https://www.fs.com/products/66240.html) to bring data to the DAQ PC/
- A 100Gbps Network Interface Card. A working example used by the DAQ group can be found at [Mellanox (Connect X-5 en)](https://www.mellanox.com/products/ethernet-adapters/connectx-5-en), however cheaper options might exist. (Any 100Gbps optical Ethernet card with a 100Gbps QSFP transceiver should work)
- A PC to receive data from the DTH via the NIC. The PC needs to be equipped with a PCIe slot to house the 100Nbps NIC
- An optical fibre with a MPO-24 connector on the DTH side and fibre optics connectors which plug into the sub-system FED on the other side. 

![MPO Samptec](images/MPO_Samptec.png){: style="display: block; margin: 0 auto;"}*Figure 1: Layout of the MPO-24 patch cord with its connector and its connection to the on-board Fireflies. This cable connects the Firefly modules with the MPO adapter mounted on the front panel. The MPO adapter is of the kind “Key-up – Key-down”.*

The custom built patch cord connecting the Fireflies to the MPO-24 Adapter on the front panel is shown in Figure1. The patch cord connects to two Firefly modules called ‘A’ and ‘B’. In the first version of the DTH-Kit two Slink Rocket channels are supported. They have to be connected to fibre positions 13 (RX0) and 24 (TX0) for the first channel and to positions 14 (RX1) and 23 (TX1) for the second Slink Rocket Channel (here RXn and TXn refer to the DTH tranceiver channels). Both are connected to the Firefly which in turn is connected to the input channel of the DAQ FPGA. These fibres have to be used by sub-detectors to transmit data to the DTH.

The patch cord mounted on the DTH has a male MPO-24 connector. Therefore the fibre bundle coming from the sub-system needs to be connected to a female MPO-24 connector. The male MPO connector from the patch-cord has two alignment pins which fit into the female connector on the sub-system cable. In addition all MPO-24 connectors have a Key on the long side which breaks the 180 degree symmetry. Together with the MPO-adapter mounted on the DTH front panel (Type “Key-up – Key-down”) this ensures that the connector can only be plugged into each other in one possible way.

![MPO Connections](images/MPO_connections.png){: style="display: block; margin: 0 auto; height: 20em;"}*Figure 2: A view on the connectors of the patch-cord connecting the Fireflies to the MPO adapter on the DTH front-panel and on the MPO connector of the sub-system. the blue lines illustrate how the fibers of both cables are interconnected. The drawing on the right hand side it should help implementors to correctly connect their SlinkRockets to the MPO connectors.*


In a later version of the DTH-Kit it is foreseen to also connect the other fibre pairs of Module A to input channels in the FPGA so that sub-systems can connect more than 2 SlinkRocket channels to the DTH-Kit. Figure shows the views onto the MPO connector of the patch cord and the MPO connector of the connecting sub-system. With this drawing it should be easy to identify how to connect the sub-system Slink Rocket transceivers.

The fibres of the Module ‘B’ are used to do loop-back tests. When the loop-back connector provided with DTH Kit is plugged onto the MPO-24 connector the fibre pairs of Module ‘A’ are connected to the pairs of Module ‘B’ as indicated in the following Table 1 and Figure 3. This arrangement allows to perform loop-back tests since the fibre pairs of Module ‘B’ are connected via a FireFly connector to SlinkRocket Sender IPs in the FPGA.

|  Module B   |  Module A   |
| ------------- |:-------------:|
|  RX4    1   |  TX0    24  |
|  RX5    2   |  TX1    23  |
|  RX6    3   |  TX2    22  |
|  RX7    4   |  TX3    21  |
|  TX7    9   |  RX3    16  |
|  TX6    10  |  RX2    15  |
|  TX5    11  |  RX1    14  |
|  TX4    12  |  RX0    13  |


![MPO-24 loopback connector.](images/OTP-215336-xx-ECUO.png){: style="display: block; margin: 0 auto;"}*Figure 3: Connection layout for the MPO-24 loopback connector.*

## Firmware

Each FPGA has a flash memory associated which memory chip has enough capacity to hold multiple firmware images. At the beginning of the memory space (lower addresses starting from `0x0`) a “golden image” is programmed. It contains a minimal firmware which allows to programme or erase firmware images on the flash memory and to load an image into the FPGA.

The golden image contains commands for the FPGA configuration logic to actually try to load a firmware image from a different position in the Flash Memory. That position should contain a firmware image with the desired functionality for the operation of the board. In case the loading of that image fails (e.g. corrupted image) this golden image will automatically be loaded by the FPGA. This ensures that the user always has the possibility to load an alternative image or to write a new image to the flash chip.

The DTH-Kit is handed out with only the golden image programmed into the flash. The user has to choose among various firmware images for operation. The firmware images can be found on the [DTH website](https://cms-daq-dth-p1.web.cern.ch/dth-p1).

### DAQ firmware images

On power up the golden image tries to load a firmware image placed at **sector 200** in the Flash chip. Hence the user needs to place the image he wants to have loaded after power up at sector 200. The firmware image can be downloaded from the [DTH web site](https://cms-daq-dth-p1.web.cern.ch/dth-p1). and programmed into the Flash memory using the `DTH_Flashy.py` utility. (See Section 5.1 and here [Flasy User Guide](https://cms-daq-dth-p1.web.cern.ch/sites/cms-daq-dth-p1.web.cern.ch/files/documents/Flashy.pdf)).

### TCDS firmware images

On power up the golden image tries to load a firmware image placed at **sector 300** in the Flash chip. (Note: this is a different position than for the DAQ FPGA.) Hence the user needs to place the image he wants to have loaded after power up at sector 300. The firmware image can be downloaded from the [DTH web site](https://cms-daq-dth-p1.web.cern.ch/dth-p1) and programmed into the Flash memory using the DTH_Flashy.py utility (See Section 5.1 and here [Flasy User Guide](https://cms-daq-dth-p1.web.cern.ch/sites/cms-daq-dth-p1.web.cern.ch/files/documents/Flashy.pdf)).

## PCI endpoints

The DAQ and the TCDS FPGAs are seen from the ComExpress embedded computer as two different PCIe endpoints. They identify to the PCI system as follows:
``` 
DAQ  FPGA: VendorID:0x10dc   DeviceID:0x0437  Index:0
TCDS FPGA: VendorID:0x10dc   DeviceID:0x0438  Index:0
```

## IPMC firmware

In case the ipmc firmware needs to be updated, the following commands need to be executed:
```
ipmitool -I lan -H <ipmc-ip-address> -U "" -P "" hpm upgrade <image> force
ipmitool -I lan -H <ipmc-ip-address> -U "" -P "" hpm activate
```
The `<image>` needs to be replaced with the filename of the new firmware.

## Software

The software for the DTH-Kit is maintained in a dedicated yum repository which is pre-installed in as a source for packages on the ComExpress boards. The location of this repository is at CERN:
- https://gitlab.cern.ch/dth_p1-v2/cmd_dth.yum

The DTH-Kit software depends on XDAQ (the hardware access for the DAQ FPGA uses the HAL library).

The software to control the DTH is running on the on-board computer on the DTH (ComExpress). This computer is booted from an image loaded to the SSD ROM of the ComExpress card. The image contains the CERN CentOS operating system and an installation of the XDAQ online software with all necessary software components installed. For your information the following paragraph contains details about the software installed. The paragraph can be skipped if you just want to use the DTH-Kit as provided.

The automatic update of the operating system software is disabled on purpose since an update of the kernel would require a recompilation of the xpci driver used by the dth software to access the hardware. In case a user wants to update the kernel he/she has to recompile and install the kernel driver. This is not recommended but in case it is desired the following procedure can be used:
- The source rpm of the driver for XDAQ 15 is available in the /root directory of the ComExpress. In case a different XDAQ release is running on the system the source rpm has to be downloaded from the XDAQ distribution pages manually. 
- Log onto the system as root and go to the directory where the source rpm is placed. Then type `rpmbuild –rebuild {name_of_source_rpm}` (Note: since this compiles a kernel driver there is no need to use the devtoolset used to build the XDAQ application software)
- This command builds a binary rpm for the running system (i.e. for the kernel which is running in this moment) which you can find in
`/root/rpmbuild/RPMS/x86`
- Installed the freshly built binary rpm you find in this directory on the machine by using `rpm -i {path_to_rpm}`

### Detailed Installation Procedure

To operate the DTH-Kit a computer running the support CERN CentOS Linux version needs to be available. On top of this the current version of cmos online software is installed. The yum groups:
- `cmsos-core`
- `msos-worksuite`
are required  to be installed. In addition the xpci kernel driver is required to allow access to the DAQ FPGA via the ComExpress PCIe bus. The worksuite contains already all necessary user space software but the kernel module needs to be compiled and installed on the ComExpress. A rpm for the released ComExpress kernel is available from the DAQ group. 



