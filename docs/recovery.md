# Recovery Procedure

In case your system becomes un-bootable or un-usable we propose the following recovery procedure. (You can also apply this if for some reason the kernel was updated and you do not succeed to boot back into the original kernel version, and you do not succeed to recompile the xdma driver for the IPBUS access. (However: older kernel versions should still be present on your system and should be bootable via grub).

The recovery procedure consists in booting a linux system on a USB stick and then transferring the DTH-KIT "factory image" to the internal ssd memory.

The following is needed to execute the procedure:
- A USB hub
- A USB memory stick with at least 32GB. You will lose all data on the stick.

The following steps should be executed for this: 

1. If your system still boots recover all files which you might want to use later. You will loose the entire content of the ssd disk on the comExpress board.
2. Download the recover image from this [CERNBOX link](https://cernbox.cern.ch/s/BpGUBrqAkGZCgTF)
3. Transfer the image to the USB stick in order to create a bootable USB stick. On linux you can do this with the dd command. You need to be sure which device corresponds to the USB stick you are using (you can inspect the system logs if in doubt). 
Make sure that the USB stick IS NOT MOUNTED: many linux systems mount usb sticks automatically.
```
dd if=dth-recovery.img of=/dev/sd{x} bs=4096 conv=fdatasync
```
x has to be replaced with the letter for your USB stick.
4. On the back side of the DTH plug the USB hub and into the hub a keyboard, a mouse and the USB stick. Plug a screen into the HDMI input
5. NOW POWER CYCLE the DTH (with the handles at the front). This is important otherwise the USB stick will not be recognized during the boot.
6. Let the system boot from the USB stick (should be the default).
If this does not work reboot the system (e.g. using the reset button close to the HDMI connector) and press ESC when the first logo is shown on the screen (you only have one second for this). In the BIOS boot sequence have a look if you find the USB device. If you do not then the USB stick is not recognized. You can power cycle again, use a different stick, or a different USB hub.
7. When the puppy-linux is booted open a terminal (right mouse button -> Utilities -> last menu option for terminal emulator).
8. mount the partition of the stick which contains the recovery image:
```
mkdir recovery
mount /dev/sda3 recovery
cd recovery
```
You should see a ~30GB large file `dth-mmcblk0.img`
9. Transfer the image to the internal ssd memory:
```
dd if=dth_mmcblk0 of=/dev/mmcblk0 bs=4096 conv=fdatasync
```
Wait a long time(>30min) for the image to be transferred.
10. Now you should be able to remove the USB stick and reboot the system in CentOS. You should have the original root and DTH accounts with the original passwords. 
