# Software Versions

| Software | Name | Version | Remarks | 
| - | - | - | - |
| Linux Operating System | CERN CentOS | 7 | Available from CERN |
| Linux kernel | `xpci` and `xdma` drivers are compiled for this specific version | 3.10.0 | Available via CERN CentOS distribution |
| XDAQ online software | `cmos_core` and `cmsos_worksuite` | 15 | Available via YUM |
| Compiler for xdaq software | Devtoolset-8 | 8 | Available via YUM |

